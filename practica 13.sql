﻿/* Práctica de SQL*/
  USE empleados;
  
/* 1. Mostrar todos los campos y todos los registros de la tabla empleado */

  SELECT emp_no,
         apellido,
         oficio,
         dir,
         fecha_alt,
         salario,
         comision,
         dept_no
    FROM emple;

/*2. Mostrar todos los campos y todos los registros de la tabla departamento */

  SELECT dept_no,
         dnombre,
         loc
    FROM depart;

/*3. Mostrar el apellido y oficio de cada empleado.*/

  SELECT apellido,oficio 
    FROM emple;

/*4. Mostrar localización y número de cada departamento.*/

  SELECT loc,dept_no
    FROM depart;

/*5. Mostrar el número, nombre y el primer caracter de la localización de cada departamento.*/

  SELECT d.dept_no,d.dnombre,LEFT(d.loc,1) 
  FROM depart d;

/*6. Indicar el número de empleados que hay.*/

  SELECT 
    COUNT(*)
    FROM emple;


/*7. Datos de los empleados ordenados por apellido de forma ascendente*/

  SELECT emp_no,
         apellido,
         oficio,
         dir,
         fecha_alt,
         salario,
         comision,
         dept_no
    FROM emple 
    ORDER BY apellido;

/*Ejercicio 8.- 	Datos de los empleados ordenados por oficio de forma ascendente 
  y después por apellido de forma descendente*/

  SELECT emp_no,
         apellido,
         oficio,
         dir,
         fecha_alt,
         salario,
         comision,
         dept_no
    FROM emple 
    ORDER BY oficio ASC, apellido DESC ;

/*9. Indicar el numero de departamentos que hay*/

  SELECT 
    COUNT(*) 
    FROM depart;

/*10. Indicar el número de empleados mas el numero de departamentos*/

  SELECT 
    (SELECT 
      COUNT(*) 
      FROM emple
     )+
    (SELECT 
      COUNT(*) 
      FROM depart
      )suma;

-- 11. Datos de los dos primeros empleados ordenados por número de departamento descendentemente.

  SELECT emp_no,
         apellido,
         oficio,
         dir,
         fecha_alt,
         salario,
         comision,
         dept_no
    FROM emple 
    ORDER BY dept_no DESC 
    LIMIT 2;

-- 12. Datos de los empleados ordenados por número de departamento descendentemente y por oficio ascendente.

  SELECT emp_no,
         apellido,
         oficio,
         dir,
         fecha_alt,
         salario,
         comision,
         dept_no
    FROM emple 
    ORDER BY dept_no DESC,oficio  ;

/* 13. Datos de los empleados ordenados por número de departamento descendentemente y por el primer caracter del 
  apellido ascendentemente.*/

  SELECT emp_no,
         apellido,
         oficio,
         dir,
         fecha_alt,
         salario,
         comision,
         dept_no
    FROM emple 
    ORDER BY dept_no DESC,LEFT(apellido,1) ASC ;

-- 14. Mostrar los códigos de los empleados cuyo salario sea mayor que 2000.

  SELECT emp_no 
    FROM emple 
    WHERE salario>2000;

-- 15. Mostrar los códigos y apellidos de los empleados cuyo salario sea menor que 2000 y no tengan comision

  SELECT emp_no,apellido 
    FROM emple 
    WHERE salario<2000 AND comision IS NULL;


-- 16. Mostrar los datos de los empleados cuyo salario este entre 1500 y 2500

    SELECT emp_no,
           apellido,
           oficio,
           dir,
           fecha_alt,
           salario,
           comision,
           dept_no
      FROM emple 
      WHERE salario BETWEEN 1500 AND 2500;

-- 17. Mostrar los datos de los empleados cuyo oficio sea "ANALISTA".

    SELECT emp_no,
           apellido,
           oficio,
           dir,
           fecha_alt,
           salario,
           comision,
           dept_no
      FROM emple 
      WHERE oficio= 'Analista';

-- 18. Mostrar los datos de los empleados cuyo oficio sea ANALISTA y ganen mas de 2000 €. 
-- Realizar con AND y con INTERSECCION

    SELECT emp_no
      FROM emple 
      WHERE oficio= 'Analista' AND salario>2000;

    SELECT * FROM 
    (SELECT e.emp_no FROM emple e WHERE e.salario>2000) C1
      NATURAL JOIN 
    (SELECT e.emp_no FROM emple e WHERE e.oficio="Analista") C2;

-- 19. Seleccionar el apellido y oficio de los empleados del departamento número 20.

  SELECT apellido,oficio 
    FROM emple 
    WHERE dept_no=20;
 
-- 20. Contar el número de empleados cuyo oficio no sea VENDEDOR

  SELECT 
    COUNT(*) 
    FROM emple 
    WHERE oficio<>'vendedor';

-- 21. Mostrar todos los datos de los empleados cuyos apellidos comiencen por m o por n ordenados por apellido de forma
-- ascendente. Realizarlo con LIKE y sin LIKE

  SELECT emp_no,
         apellido,
         oficio,
         dir,
         fecha_alt,
         salario,
         comision,
         dept_no
    FROM emple 
    WHERE apellido LIKE 'm%' OR apellido LIKE 'n%' 
    ORDER BY apellido;

  SELECT e.emp_no,
         e.apellido,
         e.oficio,
         e.dir,
         e.fecha_alt,
         e.salario,
         e.comision,
         e.dept_no 
  FROM emple e 
  WHERE LEFT(e.apellido,1) IN ("m","n") ORDER BY e.apellido;

-- 22. Seleccionar los empleados cuyo oficio sea 'VENDEDOR'. Mostrar los datos ordenados por apellido de forma ascendente.

  SELECT emp_no,
         apellido,
         oficio,
         dir,
         fecha_alt,
         salario,
         comision,
         dept_no
    FROM emple 
    WHERE oficio='vendedor' 
    ORDER BY apellido;
                                                      
-- 23. Mostrar los apellidos del empleado que mas gana.
  SELECT apellido 
    FROM emple 
    WHERE salario=(
      SELECT MAX(salario) 
        FROM emple
      );
  
  -- Otra opción más rápida,si no hay repetidos de salario:

    SELECT apellido 
      FROM emple
      ORDER BY salario
      LIMIT 1;

-- 24. Mostrar el codigo de los empleados cuyo departamento sea 20 y cuyo oficio sea 'ANALISTA'. Ordenar el resultado por apellido y oficio
-- de forma ascendente. Realizar con interseccion y con AND
  
  SELECT e.emp_no FROM emple e
    WHERE dept_no=20 AND oficio='Analista' 
    ORDER BY apellido,oficio;


  SELECT * FROM 
  (SELECT e.emp_no FROM emple e
    WHERE dept_no=20 
    ORDER BY apellido,oficio
    ) c1
    NATURAL JOIN 
  (SELECT e.emp_no FROM emple e
    WHERE oficio='Analista'
    ORDER BY apellido,oficio
    ) c2 ;
    

-- 25. Realizar un listado de los distintos meses en que los empleados se han dado de alta.

  SELECT DISTINCT MONTH(fecha_alt) 
    FROM emple ; -- Si me está pidiendo el número de mes.

  SELECT DISTINCT MONTHNAME(fecha_alt)
    FROM emple ; -- Si me está pidiendo el nombre del mes.

-- 26. Realizar un listado de los distintos años en que los empleados se han dado de alta 

    SELECT DISTINCT YEAR(fecha_alt)
      FROM emple;

-- 27. Realizar un listado de los distintos días del mes en que los empleados se han dado de alta
    
    SELECT DISTINCT DAYOFMONTH(fecha_alt)
      FROM emple;

-- 28. Mostrar los apellidos de los empleados que tengan un salario mayor que 2000 
-- o que pertenezcan al departamento número 20. Con OR y CON UNION
  
  SELECT DISTINCT apellido 
    FROM emple 
    WHERE salario>2000 OR dept_no=20;

  SELECT DISTINCT e.apellido FROM emple e WHERE e.salario>2000
    UNION
  SELECT DISTINCT e.apellido FROM emple e WHERE e.dept_no=20;


