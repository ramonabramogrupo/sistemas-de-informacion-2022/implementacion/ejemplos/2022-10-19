﻿set NAMES 'utf8';
USE ciclistas;

-- 1.1	Nombre y edad de los ciclistas que 
--      NO han ganado etapas

  -- solucion 1
  SELECT 
      DISTINCT c.nombre,c.edad
    FROM ciclista c 
      LEFT JOIN etapa e ON c.dorsal = e.dorsal
    WHERE e.dorsal IS NULL;

  -- solucion 2
  SELECT 
      DISTINCT c.nombre,c.edad
    FROM ciclista c 
      LEFT JOIN etapa e ON c.dorsal = e.dorsal
    WHERE e.numetapa IS NULL;

  
  
  -- utilizar consulta optimizada con algebra (resta)
  
  -- c1: ganadores de etapa
  SELECT DISTINCT e.dorsal FROM etapa e;

  -- c2: dorsal de los ciclistas
  SELECT c.dorsal FROM ciclista c;

  -- c3 : voy a restar c2-c1
    
    -- con la clausula except
    SELECT c.dorsal FROM ciclista c
      EXCEPT
    SELECT DISTINCT e.dorsal FROM etapa e;     

    -- como no tengo except utilizo left join
    SELECT 
        c2.dorsal 
      FROM 
        (
          SELECT c.dorsal FROM ciclista c
        ) c2
        LEFT JOIN 
        (
          SELECT DISTINCT e.dorsal FROM etapa e
        ) c1 USING(dorsal)
      WHERE 
        c1.dorsal IS NULL;

    -- como no tengo except utilizo where not in
     SELECT 
        dorsal 
      FROM 
        (
          SELECT c.dorsal FROM ciclista c
        ) c2
      WHERE 
        dorsal NOT IN 
        (
          SELECT DISTINCT e.dorsal FROM etapa e
        );
        

-- PARA TERMINAR LA CONSULTA 
-- TENGO QUE COMBINAR C3 CON CICLISTAS

  SELECT 
      c.nombre,c.edad 
    FROM ciclista c
      JOIN 
    (
        SELECT 
            c2.dorsal 
          FROM 
            (
              SELECT c.dorsal FROM ciclista c
            ) c2
            LEFT JOIN 
            (
              SELECT DISTINCT e.dorsal FROM etapa e
            ) c1 USING(dorsal)
          WHERE 
            c1.dorsal IS NULL
    ) C3 USING (dorsal);


  -- SOLUCION OPTIMIZADA 2 PERO SIN UTILIZAR LA RESTA
  -- CICLISTAS - C1
  -- UTILIZAMOS DIRECTAMENTE UN LEFT JOIN

  
   -- c1: ganadores de etapa
  SELECT DISTINCT e.dorsal FROM etapa e;
  

  -- SI LO REALIZO CON EXCEPT ME DA ERROR
    SELECT * FROM ciclista c
      EXCEPT 
    SELECT DISTINCT e.dorsal FROM etapa e;

 -- COMO LA RESTA LA TENGO QUE IMPLEMENTA CON
 -- LEFT JOIN UTILIZO LA TABLA CICLISTA EN VEZ DE C2
  SELECT 
      c.nombre,c.edad 
    FROM ciclista c
      LEFT JOIN 
          (
             SELECT DISTINCT e.dorsal FROM etapa e
          )C1 USING(dorsal)
    WHERE 
      c1.dorsal IS NULL;

  -- SOLUCION OPTIMIZADA 3 PERO SIN UTILIZAR LA RESTA
  -- CICLISTAS - C1
  -- UTILIZAMOS DIRECTAMENTE NOT IN

  
   -- c1: ganadores de etapa
  SELECT DISTINCT e.dorsal FROM etapa e;
  
    
 -- COMO LA RESTA LA TENGO QUE IMPLEMENTAR CON
 -- NOT IN UTILIZO LA TABLA CICLISTA EN VEZ DE C2
  SELECT 
      c.nombre,c.edad 
    FROM ciclista c
    WHERE 
      c.dorsal NOT IN
        (
          SELECT DISTINCT e.dorsal FROM etapa e
        );


-- 1.2	Nombre y edad de los ciclistas que NO han ganado puertos

-- solucion sin optimizar
SELECT 
    c.nombre,c.edad 
  FROM 
    ciclista c 
      LEFT JOIN puerto p ON c.dorsal = p.dorsal
  WHERE 
    p.nompuerto IS NULL ;

-- SOLUCIONES OPTIMIZADAS

-- SOLUCION 1
-- UTILIZANDO LEFT JOIN CON SUBCONSULTA DE PUERTOS

-- C1
-- CICLISTAS QUE HAN GANADO PUERTOS
SELECT DISTINCT p.dorsal FROM puerto p;

SELECT * FROM ciclista c;

-- TERMINO LA CONSULTA
SELECT 
    c.nombre,c.edad 
  FROM 
    ciclista c LEFT JOIN
      (
        SELECT DISTINCT p.dorsal FROM puerto p
      ) C1 USING(dorsal)
  WHERE
      C1.dorsal IS NULL;

-- SOLUCION OPTIMIZADA 2
-- UTILIZAR LA RESTA 

-- C1
-- CICLISTAS QUE HAN GANADO PUERTOS
SELECT DISTINCT p.dorsal FROM puerto p;

-- C2
-- CICLISTAS
SELECT  c.dorsal FROM ciclista c;

-- C3
-- resta de C2-C1

-- implementacion mediante EXCEPT
SELECT  c.dorsal FROM ciclista c
  EXCEPT 
SELECT DISTINCT p.dorsal FROM puerto p;

-- implementacion mediante LEFT JOIN IS NULL
SELECT 
    C2.dorsal
  FROM 
      (
        SELECT  c.dorsal FROM ciclista c
      ) C2
    LEFT JOIN 
      (
        SELECT DISTINCT p.dorsal FROM puerto p
      ) C1 USING(dorsal)
  WHERE 
    C1.dorsal IS NULL;

-- termino la consulta con un join
-- de ciclistas con C3

  SELECT 
      c.nombre,c.edad 
    FROM ciclista c
      JOIN 
        (
          SELECT 
              C2.dorsal
            FROM 
                (
                  SELECT  c.dorsal FROM ciclista c
                ) C2
              LEFT JOIN 
                (
                  SELECT DISTINCT p.dorsal FROM puerto p
                ) C1 USING(dorsal)
            WHERE 
              C1.dorsal IS NULL
        ) c3 USING(dorsal);



-- 1.3	Listar el director de los equipos que tengan ciclistas 
--      que NO hayan ganado NINGUNA etapa

-- directamente
SELECT 
    DISTINCT e.director 
  FROM equipo e 
    JOIN ciclista c ON e.nomequipo = c.nomequipo      
    LEFT JOIN etapa e1 ON c.dorsal=e1.dorsal
  WHERE
    e1.dorsal IS NULL;

-- SOLUCION OPTIMIZADA 1
-- UTILIZANDO LEFT JOIN DIRECTAMENTE 

-- c1
-- equipos que no han ganado etapas
SELECT 
   DISTINCT c.nomequipo
  FROM 
    ciclista c LEFT JOIN etapa e USING(dorsal)
  WHERE 
    e.dorsal IS NULL;

-- termino la consulta combinando con equipos
SELECT 
    DISTINCT e.director 
  FROM 
    equipo e JOIN 
      (
        SELECT 
           DISTINCT c.nomequipo
          FROM 
            ciclista c LEFT JOIN etapa e USING(dorsal)
          WHERE 
            e.dorsal IS NULL        
      ) c1 USING(nomequipo);


-- SOLUCION OPTIMIZADA 2
-- UTILIZANDO LA RESTA

-- C1
-- ciclistas que han ganado etapas
SELECT DISTINCT e.dorsal FROM etapa e;

-- C2
-- ciclistas
SELECT c.dorsal FROM ciclista c;

-- C3
-- C2-C1

-- implementamos con left join is null
SELECT 
    c2.dorsal 
  FROM 
      (
        SELECT c.dorsal FROM ciclista c
      ) C2
    LEFT JOIN
      (
        SELECT DISTINCT e.dorsal FROM etapa e
      ) C1 USING(dorsal)
  WHERE 
      c1.dorsal IS NULL;


-- C4
-- los equipos donde corren ciclistas
-- que no han ganado etapas
SELECT 
    DISTINCT c.nomequipo 
  FROM ciclista c JOIN 
    (
      SELECT 
          c2.dorsal 
        FROM 
            (
              SELECT c.dorsal FROM ciclista c
            ) C2
          LEFT JOIN
            (
              SELECT DISTINCT e.dorsal FROM etapa e
            ) C1 USING(dorsal)
        WHERE 
            c1.dorsal IS NULL
    ) C3 USING(dorsal);

-- termino la consulta
-- combinando equipo con C4

SELECT 
    DISTINCT e.director 
  FROM equipo e JOIN 
    (
      SELECT 
          DISTINCT c.nomequipo 
        FROM ciclista c JOIN 
          (
            SELECT 
                c2.dorsal 
              FROM 
                  (
                    SELECT c.dorsal FROM ciclista c
                  ) C2
                LEFT JOIN
                  (
                    SELECT DISTINCT e.dorsal FROM etapa e
                  ) C1 USING(dorsal)
              WHERE 
                  c1.dorsal IS NULL
          ) C3 USING(dorsal)
    ) c4 USING(nomequipo);
 

-- 1.4	Dorsal y nombre de los ciclistas que 
--      NO hayan llevado algún maillot

 SELECT 
    c.dorsal,c.nombre
  FROM 
    ciclista c LEFT JOIN lleva l ON c.dorsal = l.dorsal
  WHERE 
    l.dorsal IS NULL;

-- SOLUCION OPTIMIZADA 1
-- UTILIZANDO RESTA

-- C1 
-- CICLISTAS
  SELECT c.dorsal FROM ciclista c;

-- C2
-- CICLISTAS QUE HAN LLEVADO MAILLOT
  SELECT DISTINCT l.dorsal FROM lleva l;

-- RESTA
-- C1-C2

  -- PARA IMPLEMENTAR LA RESTA UTILIZO LEFT JOIN IS NULL
  SELECT 
      c1.dorsal  
    FROM 
        (
          SELECT c.dorsal FROM ciclista c
        ) C1
      LEFT JOIN 
        (
          SELECT DISTINCT l.dorsal FROM lleva l
        ) C2 USING(dorsal)
    WHERE 
      c2.dorsal IS NULL;


  -- PARA IMPLEMENTAR LA RESTA UTILIZO NOT IN
  SELECT 
      c1.dorsal 
    FROM 
      (
        SELECT c.dorsal FROM ciclista c    
      ) C1
    WHERE 
      c1.dorsal NOT IN 
        (
           SELECT DISTINCT l.dorsal FROM lleva l
        );

-- TERMINAR LA CONSULTA
-- COMBINAR RESTA CON CICLISTA
  SELECT 
      c.dorsal,c.nombre 
    FROM 
      ciclista c JOIN 
        (
           SELECT 
                c1.dorsal  
              FROM 
                  (
                    SELECT c.dorsal FROM ciclista c
                  ) C1
                LEFT JOIN 
                  (
                    SELECT DISTINCT l.dorsal FROM lleva l
                  ) C2 USING(dorsal)
              WHERE 
                c2.dorsal IS NULL
        ) RESTA USING(dorsal);

  -- SOLUCION OPTIMIZADA 2
  -- UTILIZANDO DIRECTAMENTE LEFT JOIN

  -- C2
  -- CICLISTAS QUE HAN LLEVADO MAILLOT
  SELECT DISTINCT l.dorsal FROM lleva l;

  -- TERMINAR LA CONSULTA
  -- COMBINACION EXTERNA ENTRE CICLISTA Y C2 UTILIZANDO IS NULL

  SELECT 
      c.dorsal,c.nombre
    FROM 
      ciclista c LEFT JOIN
        (
          SELECT DISTINCT l.dorsal FROM lleva l
        ) C2 USING(dorsal)
    WHERE 
      c2.dorsal IS NULL;

 
-- 1.5	Dorsal y nombre de los ciclistas que 
-- NO hayan llevado el maillot amarillo NUNCA

-- C1
-- CICLISTAS QUE HAN LLEVADO EL MAILLOT AMARILLO
SELECT 
    DISTINCT c.dorsal 
  FROM ciclista c
    JOIN lleva l ON c.dorsal = l.dorsal
    JOIN maillot m ON l.código = m.código
  WHERE m.color="amarillo";

-- TERMINO LA CONSULTA
-- COMBINACION EXTERNA CON IS NULL ENTRE CICLISTAS Y C1
SELECT 
    c.dorsal,c.nombre 
  FROM 
    ciclista c LEFT JOIN
      (
        SELECT 
            DISTINCT c.dorsal 
          FROM ciclista c
            JOIN lleva l ON c.dorsal = l.dorsal
            JOIN maillot m ON l.código = m.código
          WHERE m.color="amarillo"  
      ) C1 USING(dorsal)
  WHERE c1.dorsal IS NULL;

-- soluciones optimizadas

-- C1
-- CODIGO MAILLOT AMARILLO
SELECT DISTINCT m.código FROM maillot m WHERE m.color="amarillo";


-- C2
-- maillot y que ciclista le ha llevado alguna vez
SELECT DISTINCT l.dorsal,l.código FROM lleva l;

-- C3
-- dorsal de los ciclistas que han llevado alguna
-- vez maillot amarillo
SELECT 
    c2.dorsal 
  FROM 
    (
      SELECT DISTINCT l.dorsal,l.código FROM lleva l
    ) c2 JOIN 
    (
      SELECT DISTINCT m.código FROM maillot m WHERE m.color="amarillo"
    ) c1 USING(código);

-- c3
-- implementado con un simple where
SELECT 
    c2.dorsal 
  FROM 
    (
      SELECT DISTINCT l.dorsal,l.código FROM lleva l
    ) c2
  WHERE 
    c2.código=(SELECT DISTINCT m.código FROM maillot m WHERE m.color="amarillo");

-- terminar la consulta 
-- mediante un left join con is null
-- ciclista - c3

SELECT 
    c.dorsal, c.nombre 
  FROM 
    ciclista c LEFT JOIN 
      (
        SELECT 
            c2.dorsal 
          FROM 
            (
              SELECT DISTINCT l.dorsal,l.código FROM lleva l
            ) c2 JOIN 
            (
              SELECT DISTINCT m.código FROM maillot m WHERE m.color="amarillo"
            ) c1 USING(código)      
      ) c3 USING(dorsal)
  WHERE 
    c3.dorsal IS NULL;


-- 1.6	Indicar el numetapa de las etapas que NO tengan puertos
  SELECT 
      e.numetapa 
    FROM etapa e LEFT JOIN 
      puerto p ON e.numetapa = p.numetapa
    WHERE
      p.numetapa IS NULL;

  -- c1
  -- etapas que tienen puerto
  SELECT DISTINCT p.numetapa FROM puerto p;

  -- TERMINO LA CONSULTA
  -- SACAR LAS ETAPAS QUE NO TIENEN PUERTO
  -- ETAPAS-C1
  -- IMPLEMENTO CON LEFT JOIN
  SELECT 
      e.numetapa 
    FROM etapa e  LEFT JOIN
      (
        SELECT DISTINCT p.numetapa FROM puerto p 
      ) C1 USING(numetapa)
    WHERE 
      c1.numetapa IS NULL;

-- 1.7	Indicar la distancia media de las etapas que NO tengan puertos

-- DIRECTAMENTE

  SELECT 
      AVG(e.kms) AS media
    FROM  
      etapa e LEFT JOIN 
        puerto p ON e.numetapa = p.numetapa
    WHERE p.numetapa IS NULL;

 -- optimizada

 -- c1
 -- etapas que tienen puerto
  SELECT DISTINCT p.numetapa FROM puerto p;

  -- TERMINO LA CONSULTA
  -- distancia media de las etapas sin puerto
  -- ETAPAS-C1
  
  
  -- IMPLEMENTO CON LEFT JOIN
  SELECT 
      AVG(e.kms) AS media
    FROM etapa e  LEFT JOIN
      (
        SELECT DISTINCT p.numetapa FROM puerto p 
      ) C1 USING(numetapa)
    WHERE 
      c1.numetapa IS NULL;

  -- IMPLEMENTO CON NOT IN
  SELECT 
      AVG(e.kms) AS media 
    FROM etapa e 
    WHERE 
      e.numetapa NOT IN (SELECT DISTINCT p.numetapa FROM puerto p);

-- 1.8	Listar el número de ciclistas que NO hayan ganado alguna etapa

-- directamente
SELECT 
    COUNT(c.dorsal) AS numero 
  FROM ciclista c LEFT JOIN 
    etapa e ON c.dorsal = e.dorsal 
  WHERE 
    e.dorsal IS NULL;

-- optimizada

-- c1
-- todos los ciclistas
SELECT c.dorsal FROM ciclista c;

-- c2
-- los ciclistas que han ganado etapas
SELECT DISTINCT e.dorsal FROM etapa e;

-- resta
-- c1-c2
-- implemento mediante left join
SELECT 
    COUNT(c1.dorsal)  AS numero
  FROM 
    (
      SELECT c.dorsal FROM ciclista c
    ) c1
  LEFT JOIN 
    (
      SELECT DISTINCT e.dorsal FROM etapa e
    ) c2 USING(dorsal)
  WHERE
    c2.dorsal IS NULL;

-- resta 
-- c1-c2
-- implementar con not exists
SELECT 
    COUNT(*) AS numero
  FROM 
    (
      SELECT c.dorsal FROM ciclista c
    ) c1
  WHERE 
    NOT EXISTS (SELECT 1 FROM etapa e WHERE e.dorsal=c1.dorsal);


-- 1.9	Listar el dorsal de los ciclistas que hayan ganado alguna etapa 
--      que no tenga puerto	


-- directamente
SELECT 
    DISTINCT e.dorsal 
  FROM ciclista c 
    JOIN etapa e ON c.dorsal = e.dorsal
    LEFT JOIN puerto p ON e.numetapa = p.numetapa
  WHERE p.numetapa IS NULL;

-- C1
-- LAS ETAPAS QUE TIENEN PUERTO
SELECT DISTINCT p.numetapa FROM puerto p;

-- RESTAR
-- ME QUEDO CON EL DORSAL DEL CICLISTA DE LA RESTA
-- ETAPA - C1

-- IMPLEMENTAR CON LEFT JOIN
SELECT 
    DISTINCT e.dorsal 
  FROM etapa e 
    LEFT JOIN 
      (
        SELECT DISTINCT p.numetapa FROM puerto p
      ) c1 USING(numetapa)
  WHERE 
    c1.numetapa IS NULL;


-- IMPLEMENTAMOS LA RESTA 
-- MEDIANTE UN ANTISEMIJOIN
SELECT 
    DISTINCT e.dorsal 
  FROM 
    etapa e
  WHERE
    NOT EXISTS (SELECT 1 FROM puerto p WHERE p.numetapa=e.numetapa);


-- IMPLEMENTAMOS LA RESTA
-- MEDIANTE NOT IN

SELECT 
    DISTINCT e.dorsal 
  FROM 
    etapa e
  WHERE
    e.numetapa NOT IN (SELECT DISTINCT p.numetapa FROM puerto p);



-- 1.10	Listar el dorsal de los ciclistas que hayan ganado 
--      UNICAMENTE etapas que no tengan puertos	


-- c1
-- etapas que no tienen puerto
SELECT DISTINCT p.numetapa FROM puerto p;

-- C2
-- dorsal de los ciclistas que han ganado etapas con puerto
SELECT 
    DISTINCT e.dorsal 
  FROM etapa e JOIN 
    (
        SELECT DISTINCT p.numetapa FROM puerto p
    ) c1 USING(numetapa);


-- c2
-- dorsal de los ciclistas que han ganado etapas con puerto
SELECT 
    DISTINCT e.dorsal 
  FROM 
    puerto p 
      JOIN etapa e ON p.numetapa = e.numetapa;

-- c3
-- ciclistas que han ganado etapas
SELECT DISTINCT e.dorsal FROM etapa e;

-- resto 
-- c3 - c2 

-- implemento con left join
SELECT 
    c3.dorsal 
  FROM 
      (
        SELECT DISTINCT e.dorsal FROM etapa e  
      ) c3
    LEFT JOIN
      (
        SELECT 
            DISTINCT e.dorsal 
          FROM etapa e JOIN 
            (
                SELECT DISTINCT p.numetapa FROM puerto p
            ) c1 USING(numetapa)      
      ) c2 USING(dorsal)
  WHERE c2.dorsal IS NULL;


-- implementada con un antisemijoin
SELECT 
    c3.dorsal 
  FROM 
    (
      SELECT DISTINCT e.dorsal FROM etapa e
    ) c3
  WHERE 
    NOT EXISTS 
      (
        SELECT 
          DISTINCT e.dorsal 
        FROM etapa e JOIN 
          (
              SELECT DISTINCT p.numetapa FROM puerto p
          ) c1 USING(numetapa)
        WHERE  e.dorsal=c3.dorsal
      );




